#!/bin/bash

script_dir=$(dirname "$BASH_SOURCE")
source $script_dir/prepare_bot.sh $1

roslaunch duckietown_demos master.launch \
veh:=$VEHICLE_NAME local:=true joystick:=false coordination:=true navigation:=true anti_instagram:=false \
intersectionType:=trafficLight \
/LED/detector:=true /LED/interpreter:=true \
visualization:=false \
/navigation/apriltags_random:=false apriltags:=false \
obstacle_avoidance:=true /obstacle_avoidance/safety:=false /obstacle_avoidance/detection:=false \
line_detector_param_file_name:=osll
