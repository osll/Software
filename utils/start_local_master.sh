#!/bin/bash

script_dir=$(dirname "$BASH_SOURCE")
source $script_dir/prepare_bot.sh $1

roslaunch duckietown_demos master.launch \
veh:=$VEHICLE_NAME local:=true joystick:=false coordination:=true navigation:=true \
intersectionType:=trafficLight \
apriltags:=true \
obstacle_avoidance:=true
