#!/bin/bash

script_dir=$(dirname "$BASH_SOURCE")
source $script_dir/prepare_bot.sh $1

export ROS_MASTER_URI="http://$VEHICLE_NAME.local:11311"
